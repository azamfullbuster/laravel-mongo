@extends('includes.master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">User</h1>
                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">User</li>
                    </ol>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <section class="content">
        <div class="col-md-12 mb-3">
            @if(Auth::user()->role == "Admin")
            <a href="{{route('users.create')}}" class="btn btn-primary">Create</a>
            @endif
        </div>
        <div class="container-fluid">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">User Table</h3>
                </div>
                <br />
                @if (\Session::has('success'))
                <div class="alert alert-success">
                    <p>{{ \Session::get('success') }}</p>
                </div><br />
                @endif
                <!-- /.card-header -->
                <div class="card-body">
                    <table id="tbl-user" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th width="10%" style="text-align: center;">Name</th>
                                <th width="15%" style="text-align: center;">Email</th>
                                <th width="15%" style="text-align: center;">Role</th>
                                <th width="20%" style="text-align: center;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($users as $index=>$user)
                            <tr>
                                <td>{{$user->name}}</td>
                                <td>{{$user->email}}</td>
                                <td>{{$user->role}}</td>
                                <td class="center">
                                    @if(Auth::user()->role == "Admin")
                                    <a href="{{route('users.edit', $user->id)}}" class="btn btn-info btn-sm">
                                        Edit
                                    </a>
                                    @endif
                                    <a href="{{route('profiles.show', $user->id)}}" class="btn btn-success btn-sm">
                                        Show Profile
                                    </a>
                                    @if(Auth::user()->role == "Admin")
                                    <form class="d-inline" action="{{route('users.destroy', $user->id)}}" method="POST"
                                        onsubmit="return confirm('Move user {{$user->name}} to trash?')">
                                        @csrf
                                        <input type="hidden" value="DELETE" name="_method">
                                        <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                                    </form>
                                    @endif
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="3" class="text-center white">No User Except You</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
</div>
<!-- /.container-fluid -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection
@push('dataTable-script')
<script>
    $.widget.bridge('uibutton', $.ui.button)
</script>
<script>
    $(function () {
            $("#tbl-user").DataTable();
        });
</script>
@endpush