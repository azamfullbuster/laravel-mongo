@extends('includes.master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">User</h1>
                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{route('users.index')}}">User</a></li>
                        <li class="breadcrumb-item active">Edit</li>
                    </ol>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Edit User</h3>
                        </div>
                        <!-- /.card-header -->
                        <form method="post" action="{{route('users.update', $id)}}">
                            @method('PUT')
                            @csrf
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="name">Full Name:</label>
                                    <input type="text" class="form-control" name="name" value="{{$users->name}}">
                                </div>
                                <div class="form-group">
                                    <label for="Model">Email:</label>
                                    <input type="text" class="form-control" name="email" value="{{$users->email}}">
                                </div>
                                @if(Auth::user()->role == "Admin")
                                <div class="form-group">
                                    <label for="role">Role:</label>
                                    <select id="role" name="role">
                                        <option value="User">User</option>
                                        @if($users->role == "Admin")
                                        <option selected value="Admin">Admin</option>
                                        @else
                                        <option value="Admin">Admin</option>
                                        @endif
                                    </select>
                                </div>
                                @else
                                <div class="form-group">
                                    <label for="role">Role:</label>
                                    <select id="role" name="role" disabled>
                                        <option value="User">User</option>
                                        @if($users->role == "Admin")
                                        <option selected value="Admin">Admin</option>
                                        @else
                                        <option value="Admin">Admin</option>
                                        @endif
                                    </select>
                                </div>
                                @endif
                                {{-- <div class="form-group">
                                    <label for="password">Password:</label>
                                    <input id="password" type="password"
                                        class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                name="password" placeholder="Password" required>
                            </div> --}}
                            <div class="card-footer">
                                <button type="submit" class="btn btn-success">Update</button>
                            </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
</div>
</section>
</div>
@endsection