<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>AdminLTE 3 | Dashboard</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSS -->
    @include('includes.css')
</head>

<body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper" id="app">
        <!-- Header -->
        @include('includes.header')
        <!-- Sidebar -->
        @include('includes.sidebar')
        @yield('content')
        <!-- Footer -->
        @include('includes.footer')

        <!-- ./wrapper -->
        <!-- JS -->
        @include('includes.js')
</body>

</html>