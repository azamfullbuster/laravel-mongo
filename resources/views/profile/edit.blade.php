@extends('includes.master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">User</h1>
                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{route('users.index')}}">User</a></li>
                        <li class="breadcrumb-item active">Edit Profile</li>
                    </ol>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Edit Profile</h3>
                        </div>
                        <!-- /.card-header -->

                        <form method="post" action="{{ route('profiles.update', $profile->_id) }}"
                            enctype="multipart/form-data">
                            @method('PUT')
                            @csrf
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="name">Full Name</label>
                                    <input type="text" class="form-control" name="name" value="{{$user->name}}">
                                </div>
                                <div class="form-group">
                                    <label for="Model">Email:</label>
                                    <input type="text" class="form-control" name="email" value="{{$user->email}}">
                                </div>
                                @if($user->role == "Admin")
                                <div class="form-group">
                                    <label for="role">Role:</label>
                                    <select id="role" name="role">
                                        <option value="User">User</option>
                                        @if($user->role == "Admin")
                                        <option selected value="Admin">Admin</option>
                                        @else
                                        <option value="Admin">Admin</option>
                                        @endif
                                    </select>
                                </div>
                                @else
                                <div class="form-group">
                                    <label for="role">Role:</label>
                                    <select id="role" name="role" disabled>
                                        <option value="User">User</option>
                                        @if($user->role == "Admin")
                                        <option selected value="Admin">Admin</option>
                                        @else
                                        <option value="Admin">Admin</option>
                                        @endif
                                    </select>
                                </div>
                                @endif
                                <div class="form-group">
                                    <label for="address">Address</label>
                                    <input id="address" type="text"
                                        class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}"
                                        name="address" value="{{$profile->address}}" placeholder="Enter Your Address"
                                        required autofocus>
                                    @if ($errors->has('address'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span> @endif
                                </div>
                                <div class="form-group">
                                    <label for="phone">Phone:</label>
                                    <input id="phone" type="number"
                                        class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}"
                                        name="phone" value="{{$profile->phone}}" placeholder="Your Phone Number">
                                </div>
                                <div class="form-group">
                                    <label for="Image">Profile Photo :</label>
                                    <br>
                                    <input type="file" class="btn btn-light" name="pict">

                                </div>
                                <div class="form-group">
                                    <div class="card-footer">
                                        <button type="submit" class="btn btn-primary">Edit</button>
                                    </div>
                                </div>


                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection