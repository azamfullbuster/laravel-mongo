@extends('includes.master')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">User</h1>
                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{route('users.index')}}">User</a></li>
                        <li class="breadcrumb-item active">Profile</li>
                    </ol>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            @if ($profile === null)
            <div class="text-center">
                <h4>User Ini Belum Membuat Profile</h4>
                @if($user_id==Auth::user()->_id)
                <a href="{{route('create_profile' , $user_id)}}" class="btn btn-primary">Buat Profile</a>
                @endif
            </div>
            @else
            <div class="card card-primary card-outline">
                <div class="card-body box-profile">
                    <div class="text-center">
                        @if ($profile->pict === '')
                        <img class="profile-user-img img-fluid img-circle" src="{{ asset('images/profile-image.jpg') }}"
                            alt="User profile picture">
                        @else
                        <img class="profile-user-img img-fluid img-circle"
                            src="{{ asset('uploads/profpict/' . $profile->pict) }}" alt="User profile picture">
                        @endif
                    </div>
                    <h3 class="profile-username text-center">{{ $user->name }}</h3>
                    <p class="profile text-center">{{ $user->email }}</p>
                    <ul class="list-group list-group-unbordered mb-3">
                        <li class="list-group-item">
                            <b>Address : </b> <a class="float-right">{{ $profile->address }}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Phone : </b> <a class="float-right">{{ $profile->phone }}</a>
                        </li>
                    </ul>
                    @if($user_id==Auth::user()->_id)
                    <a href="{{ route('profiles.edit', $profile->_id) }}" class="btn btn-primary btn-block">
                        <b>Edit Profile</b>
                    </a>
                    @endif


                </div>
                <!-- /.card-body -->
            </div>
            @endif
    </section>
</div>
@endsection