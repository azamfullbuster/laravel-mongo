<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

Route::resources([
    'users' => 'UserController',
]);

Route::resources([
    'profiles' => 'ProfileController',
]);

Route::get('create_profile/{user_id}', [
    'as' => 'create_profile',
    'uses' => 'ProfileController@create'
]);

Route::post('store_profile/{user_id}', [
    'as' => 'store_profile',
    'uses' => 'ProfileController@store'
]);
