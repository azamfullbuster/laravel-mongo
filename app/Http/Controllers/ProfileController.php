<?php

namespace App\Http\Controllers;

use App\Profile;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth')->only(['index', 'create', 'store', 'edit', 'update', 'destroy']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $user_id = $id;

        return view('profile.create', compact('user_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $profile = new Profile();
        $profile->address = $request->get('address');
        $profile->phone = $request->get('phone');
        if ($request->hasFile('pict')) {
            $file = $request->file('pict');
            $extension = $file->getClientOriginalExtension();
            $filename = time() . '.' . $extension;
            $file->move('uploads/profpict', $filename);
            $profile->pict = $filename;
        } else {
            /* return $request; */
            $profile->pict = '';
        }

        $profile->user_id = $id;
        $profile->save();

        return redirect('users')->with('success', 'Profile has been  successfully added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $profile = Profile::where('user_id', $id)->first();
        $user_id = $id;
        $user = User::where('_id', $id)->first();
        return view('profile.show', compact('profile', 'user_id', 'user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $profile = Profile::find($id);
        $user = $profile->user;

        return view('profile.edit', compact('profile', 'user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $profile = Profile::find($id);
        $user = $profile->user;

        if ($request->hasFile('pict')) {
            $file = $request->file('pict');
            $extension = $file->getClientOriginalExtension();
            $filename = time() . '.' . $extension;
            $file->move('uploads/profpict', $filename);

            $user->name = $request->get('name');
            $user->email = $request->get('email');
            $user->role = $request->get('role');

            $profile->pict = $filename;
            $profile->address = $request->get('address');
            $profile->phone = $request->get('phone');
        } else {
            /* return $request; */
            $filename = $profile->pict;

            $user->name = $request->get('name');
            $user->email = $request->get('email');
            $user->role = $request->get('role');

            $profile->pict = $filename;
            $profile->address = $request->get('address');
            $profile->phone = $request->get('phone');
        }

        $profile->save();
        $user->save();
        return redirect('users')->with('success', 'Profile Edited');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
