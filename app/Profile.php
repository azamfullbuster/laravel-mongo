<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Profile extends Eloquent
{
    protected $collection = 'profiles';

    protected $fillable = [
        'address', 'phone', 'pict', 'user_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
